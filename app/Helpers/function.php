<?php
function url_link(){
   $data='';

   return $data;
}
function bulan($bulan)
{
   Switch ($bulan){
      case '01' : $bulan="Januari";
         Break;
      case '02' : $bulan="Februari";
         Break;
      case '03' : $bulan="Maret";
         Break;
      case '04' : $bulan="April";
         Break;
      case '05' : $bulan="Mei";
         Break;
      case '06' : $bulan="Juni";
         Break;
      case '07' : $bulan="Juli";
         Break;
      case '08' : $bulan="Agustus";
         Break;
      case '09' : $bulan="September";
         Break;
      case 10 : $bulan="Oktober";
         Break;
      case 11 : $bulan="November";
         Break;
      case 12 : $bulan="Desember";
         Break;
      }
   return $bulan;
}

function bln($id){
   if($id>9){
      $data=$id;
   }else{
      $data='0'.$id; 
   }

   return $data;
}

function uang($id){
   $data=number_format($id,0);

   return $data;
}

function periode(){
   $data=App\Periode::orderBy('id','Desc')->get();
   return $data;
}

function periode_sts(){
   $data=App\Periode::orderBy('id','Desc')->get();
   return $data;
}

function kelompok(){
   $data=App\Kelompok::orderBy('id','Desc')->get();
   return $data;
}


function dampak(){
   $data=App\Dampak::orderBy('id','Desc')->get();
   return $data;
}
function cek_kriteria($id){
   $data=App\Kriteria::where('id',$id)->first();
   return $data;
}

function get_kriteria($dampakid,$kategoriid,$level){
   $data=App\Kriteria::where('dampak_id',$dampakid)->where('kategori_id',$kategoriid)->where('level',$level)->get();
   return $data;
}
function kriteria($dampakid,$kategoriid){
   $data=App\Kriteria::where('dampak_id',$dampakid)->where('kategori_id',$kategoriid)->whereNotIn('id',[10,11,12,13,15])->get();
   return $data;
}
function klasifikasi(){
   $data=App\Klasifikasi::orderBy('id','Desc')->get();
   return $data;
}

function peluang(){
   $data=App\Peluang::orderBy('id','Desc')->get();
   return $data;
}
function kategori(){
   $data=App\Kategori::orderBy('id','Asc')->get();
   return $data;
}

function get_kpi($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->orderBy('kode','Asc')->get();
   return $data;
}
function get_alasan($unit,$periode,$role){
   $data=App\Alasan::where('unit_id',$unit)->where('role_id',$role)->where('periode_id',$periode)->where('sts',0)->orderBy('id','Desc')->get();
   return $data;
}
function jum_alasan($id,$role){
	if($role==1){
					$data=App\Alasan::where('risikobisnis_id',$id)->where('sts',0)->count();
				}else{
					$data=App\Alasan::where('risikobisnis_id',$id)->where('role_id',$role)->where('sts',0)->count();
					}
   return $data;
}

function jum_alasan_1($id,$role){
   $data=App\Alasan::where('risikobisnis_id',$id)->whereIn('role_id',[1,2,3,4])->where('sts',0)->count();
   //$data=App\Alasan::where('risikobisnis_id',$id->where('sts',0)->count();
   return $data;
}

function periode_aktif(){
   $data=App\Periode::where('sts_aktif',1)->first();
   return $data;
}

function periode_berikutnya($tahun){
   $data=App\Periode::where('urut','>',1)->where('tahun',$tahun)->get();
   return $data;
}

function matrik($peluang,$dampak){
   $data=App\Matrik::where('peluang_id',$peluang)->where('dampak_id',$dampak)->first();
   return $data;
}

function total_kpi_unit($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
   return $data;
}
function total_risiko($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
   return $data;
}
function total_risiko_validasi($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('sts','>',1)->count();
   return $data;
}
function total_risiko_validasi_kp($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[1,2,3,4])->count();
   return $data;
}

function total_kpi_unit_utama($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('level',1)->where('deleted','0')->count();
   return $data;
}

function total_kpi_unit_proses($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('sts',1)->where('deleted','0')->count();
   return $data;
}

function total_kpi_unit_keypersson($unit,$periode, $jmlkpi){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('sts','0')->groupBy('kpi_id')->count();   
   return $data;
}

function total_kpi_unit_verifikatur($unit, $periode, $jmlkpi){
   $kpi=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[1,2,3,4,5,7,8,9,10])->groupBy('kpi_id')->get();
   $data = count($kpi);
   return $data;
}

function total_kpi_unit_pimpinan($unit,$periode, $jmlkpi){
   $kpi=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[2,3,4,5,8,9,10])->groupBy('kpi_id')->get();
   $data = count($kpi);
   return $data;
}

function total_kpi_unit_palingutama($unit,$periode){
   $data=App\Kpi::where('unit_id',$unit)->where('periode_id',$periode)->where('level',2)->where('deleted','0')->count();
   return $data;
}

function cek_sumber_risiko($id){
   $data=App\Sumber::where('risikobisnis_id',$id)->count();
   return $data;
}

function trk_keyperson($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   $acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[1,2,3,4,5])->count();
   if($data==0){
      $tampil='<a class="btn btn-default btn-social btn-twitter btn-xs" style="background:#fff;color:#000"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
   }else{
      if($acc==$data){
         $tampil='<span class="btn btn-success btn-xs"><i class="fa fa-check"></i></span>';
      }else{
         $tampil='<a class="btn btn-block btn-social btn-twitter btn-xs"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
      }
   }
  
   
   return $tampil;
}
function trk_verifikatur($unit,$periode){
   //$data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   //$acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('sts','>',1)->count();
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   //$acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[2,3,4,5])->groupBy('kpi_id')->count();
   $acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('date_vk','!=','')->count();
   if($data==0){
      $tampil='<a class="btn btn-default btn-social btn-twitter btn-xs" style="background:#fff;color:#000"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
   }else{
      if($acc==$data){
         $tampil='<span class="btn btn-success btn-xs"><i class="fa fa-check"></i></span>';
      }else{
         $tampil='<a class="btn btn-block btn-social btn-twitter btn-xs"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
      }
   }
   
   return $tampil;
}
function trk_pimpinanunit($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   $acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[3,4,5])->count();
   if($data==0){
      $tampil='<a class="btn btn-default btn-social btn-twitter btn-xs" style="background:#fff;color:#000"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
   }else{
      if($acc==$data){
         $tampil='<span class="btn btn-success btn-xs"><i class="fa fa-check"></i></span>';
      }else{
         $tampil='<a class="btn btn-block btn-social btn-twitter btn-xs"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
      }
   }
   
   return $tampil;
}
function trk_mgrlrc($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   $acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[5])->count();
   if($data==0){
      $tampil='<a class="btn btn-default btn-social btn-twitter btn-xs" style="background:#fff;color:#000"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
   }else{
      if($acc==$data){
         $tampil='<span class="btn btn-success btn-xs"><i class="fa fa-check"></i></span>';
      }else{
         $tampil='<a class="btn btn-block btn-social btn-twitter btn-xs"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
      }
   }
   
   return $tampil;
}
function trk_pimpinansubdit($unit,$periode){
   $data=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
   $acc=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->whereIn('sts',[5])->count();
   if($data==0){
      $tampil='<a class="btn btn-default btn-social btn-twitter btn-xs" style="background:#fff;color:#000"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
   }else{
      if($acc==$data){
         $tampil='<span class="btn btn-success btn-xs"><i class="fa fa-check"></i></span>';
      }else{
         $tampil='<a class="btn btn-block btn-social btn-twitter btn-xs"><i class="fa fa-gear"></i> &nbsp;&nbsp;('.$acc.' / '.$data.')&nbsp;&nbsp; </a>';
      }
   }
   
   return $tampil;
}
//cekcccccc
function unit(){
   $data=App\Unit::orderBy('nama','Desc')->get();
   return $data;
}
function unit_subdit(){
   $data=App\Unit::where('sts_unit',1)->orderBy('nama','Desc')->get();
   return $data;
}
function unit_bawahan_subdit($id){
   $unit=substr($id,0,2);
   $data=App\Unit::where('objectabbr','LIKE','%'.$unit.'%')->orderBy('objectabbr','Asc')->get();
   return $data;
}
function count_unit_bawahan_subdit_divisi($id){
   $unit=substr($id,0,2);
   $data=App\Unit::where('objectabbr','LIKE','%'.$unit.'%')->where('objectabbr','!=',$id)->count();
   $hsl=($data/4);
   return (round($hsl)+1);
}
function count_get_unit_pimpinansubdit($id){
   $unit=substr($id,0,2);
   $data=App\Unit::where('objectabbr','LIKE',$unit.'%')->where('objectabbr','!=',$id)->count();
   $hsl=($data/4);
   return (round($hsl)+1);
}
function unit_bawahan_subdit_divisi($id){
   $unit=substr($id,0,2);
   $data=App\Unit::where('objectabbr','LIKE','%'.$unit.'%')->where('objectabbr','!=',$id)->orderBy('objectabbr','Asc')->get();
   return $data;
}
function cek_unit($id){
   $data=App\Unit::where('objectabbr',$id)->first();
   return $data;
}
function cek_periode($id){
   $data=App\Periode::where('id',$id)->first();
   return $data;
}
function get_sumber($id){
   $data=App\Sumber::where('risikobisnis_id',$id)->get();
   return $data;
}
function jum_sumber($id){
   $data=App\Sumber::where('risikobisnis_id',$id)->count();
   return $data;
}
function cek_sumber($id,$urut){
   $data=App\Sumber::where('risikobisnis_id',$id)->where('urut',$urut)->first();
   return $data;
}

function cek_kaidah($id){
   if($id==0){
      $data='<span class="btn btn-default btn-xs" title="Proses"><i class="fa fa-gear"></i></span>';
   }
   if($id==1){
      $data='<span class="btn btn-primary btn-xs" title="setuju"><i class="fa fa-thumbs-up"></i></span>';
   }
   if($id==2){
      $data='<span class="btn btn-success btn-xs" title="Tidak setuju"><i class="fa fa-thumbs-down"></i></span>';
   }
   return $data;
}

function cek_kaidah_verifikatur($id,$no){
   if($id==0){
      $data='<span class="btn btn-default btn-xs" onclick="cek_kaidah('.$no.')" title="Proses"><i class="fa fa-gear"></i></span>';
   }
   if($id==1){
      $data='<span class="btn btn-primary btn-xs" onclick="cek_kaidah('.$no.')"title="setuju"><i class="fa fa-thumbs-up"></i></span>';
   }
   if($id==2){
      $data='<span class="btn btn-success btn-xs" onclick="cek_kaidah('.$no.')" title="Tidak setuju"><i class="fa fa-thumbs-down"></i></span>';
   }
   return $data;
}

function role(){
   $data=App\Role::orderBy('name','Desc')->get();
   return $data;
}

function cek_level($id){
   $data=App\Level::where('id',$id)->first();
   if($id==0){
      $tam='<span class="label label-default"><i>Standar</i></span>';
   }else{
      $tam='<span class="label label-'.$data['warna'].'"><b>'.$data['nama'].'</b></span>';
   }
   return $tam;
}

function cek_role($id){
   if($id==0){
      $tam='<span class="btn btn-default"><i class="fa fa-remove"></i></span>';
   }else{
      $tam='<span class="btn btn-success"><i class="fa fa-check"></i></span>';
   }
   return $tam;
}

function admin(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',5)->count();
   return $data;
}

function verifikatur(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',2)->count();
   return $data;
}

function pimpinanunit(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',3)->count();
   return $data;
}

function unit_pimpinanunit(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',3)->get();
   return $data;
}

function keyperson(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',1)->count();
   return $data;
}

function pimpinangcg(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',4)->count();
   return $data;
}

function unit_keyperson(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',1)->get();
   return $data;
}

function pimpinansubdit(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',6)->count();
   return $data;
}

function unit_pimpinansubdit(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',6)->get();
   return $data;
}
function get_unit_pimpinansubdit($id){
   $ide=substr($id,0,2);
   $data=App\Unit::where('objectabbr','LIKE',$ide.'%')->get();
   return $data;
}

function managergcg(){
   $data=App\Hasrole::where('kode',Auth::user()['kode'])->where('role_id',4)->count();
   return $data;
}

function cek_ttd($unit,$role){
	if($unit==''){
		$data=App\Hasrole::where('role_id',$role)->first();
	}else{
		$data=App\Hasrole::where('unit_id',$unit)->where('role_id',$role)->first();
	}
	
   $user = App\User::where('kode',$data->kode)->first();
   return $user;
}
 function hariini(){
	 date_default_timezone_set('Asia/Jakarta');
	 $data=date("Y-m-d H:i:s");
	 return $data;
 }
 
 function name_role(){
   $data=App\Hasrole::select('roles.name')
   ->leftjoin('roles','roles.id','=','has_role.role_id')
   ->where('kode',Auth::user()['kode'])->groupBy('roles.id')
   ->get();
   return $data;
}


function personal_unit(){
	$data = App\Hasrole::select('unit.id','unit.kode','unit.objectabbr','unit.nama','unit.kodecc','unit.namacc','unit.creator','unit.modifier','unit.created_at','unit.updated_at','unit.sts','unit.sts_unit','unit.singkatan','has_role.unit_id')
	->leftjoin('unit','has_role.unit_id','=','unit.objectabbr')
   ->where('has_role.unit_id','<>','')
   ->where('has_role.kode',Auth::user()['kode'])->groupBy('has_role.unit_id')
   ->get();
	return $data;
}
function get_unitpersonal($unit_id){
	$data=App\Unit::whereIn('objectabbr',[$unit_id])->get();
   return $data;
}
function data_risiko($unit){
	$risiko=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',periode_aktif()['id'])->count();
}

function stage_keyperson($unit,$periode){
	$jmlkpi = App\Kpi ::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
	$jmlrisiko=App\Risikobisnis::where('unit_id',$unit)->groupBy('kpi_id')->where('periode_id',$periode)->get();   
	$risiko=count($jmlrisiko);
	
	if($jmlkpi==$risiko){
		$datarisiko=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
		$stagekp = App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('date_kp','!=','')->count();
			if($datarisiko==$stagekp){
					$data = $jmlkpi;
				}else{
					$data = ($jmlkpi)-($risiko);
				}
		
	}else{
		$data = ($jmlkpi)-($risiko);
	}
	
	
	return $data;
}
function stage_vk($unit,$periode){
	$jmlkpi = App\Kpi ::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
	$jmlrisiko=App\Risikobisnis::where('unit_id',$unit)->groupBy('kpi_id')->where('periode_id',$periode)->get();   
	$risiko=count($jmlrisiko);
	
	if($jmlkpi==$risiko){
		$datarisiko=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
		$stagevk = App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->count();
		if($datarisiko==$stagevk){
					$data = $jmlkpi;
				}else{
					$data = ($jmlkpi)-($risiko);
				}
		
	}else{
		$data = ($jmlkpi)-($risiko);
	}
	
	
	return $data;
}

function stage_manager($unit,$periode){
	$jmlkpi = App\Kpi ::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
	$jmlrisiko=App\Risikobisnis::where('unit_id',$unit)->groupBy('kpi_id')->where('periode_id',$periode)->get();   
	$risiko=count($jmlrisiko);
	
	if($jmlkpi==$risiko){
		$datarisiko=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
			$stagemgr = App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->where('date_mgr','!=','')->count();
			if($datarisiko==$stagemgr){
							$data = $jmlkpi;
						}else{
							$data = ($jmlkpi)-($risiko);
						}
				
			}else{
				$data = ($jmlkpi)-($risiko);
			}
	
	return $data;
}
function stage_upmr($unit,$periode){
	$jmlkpi = App\Kpi ::where('unit_id',$unit)->where('periode_id',$periode)->where('deleted','0')->count();
	$jmlrisiko=App\Risikobisnis::where('unit_id',$unit)->groupBy('kpi_id')->where('periode_id',$periode)->get();   
	$risiko=count($jmlrisiko);
	
	if($jmlkpi==$risiko){
		$datarisiko=App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->count();
		$stageupmr = App\Risikobisnis::where('unit_id',$unit)->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->where('date_mgr','!=','')->where('date_upmr','!=','')->count();
		if($datarisiko==$stageupmr){
							$data = $jmlkpi;
						}else{
							$data = ($jmlkpi)-($risiko);
						}
				
			}else{
				$data = ($jmlkpi)-($risiko);
			}
	
	return $data;
	
}



function stage($periode){
	
           echo"
            <style>
                th{
                    font-size:12px;
                    background:transparent;;
                    border:solid 0px #000;
                    text-align:center;
                    padding:5px;
                    text-transform:uppercase;
					color: black;
                    
                }
                td{
                    font-size:12px;
                    border:solid 0px #000;
                    vertical-align:top;
                    
                }
				td.col-progres{
                    float:left;
                    margin:0.5%;
                    width:18.8%;
                    text-align:center;
                    font-size: 12px;
                    font-weight: bold;
                    height:60px;
                    color:#fff;
                    background-image:url('img/proses.png');
                    background-repeat: no-repeat;
                    background-size:100%;
					padding-top:10px;
					
                }
                td.col-selesai{
                    float:left;
                    padding-top:23px;
                    margin:0.5%;
                    width:18%;
                    text-align:center;
                    font-size: 12px;
                    font-weight: bold;
                    height:60px;
                    color:#fff;
                    background-image:url('img/selesai.png');
                    background-repeat: no-repeat;
                    background-size:100%;
                }
            </style>";
		foreach(personal_unit() as $uni){
			              
                $jmlkpi = App\Kpi ::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('deleted','0')->count();
				$jmlrisiko=App\Risikobisnis::where('unit_id',$uni['objectabbr'])->groupBy('kpi_id')->where('periode_id',$periode)->get();   
				$risiko=count($jmlrisiko);
				
				if($jmlkpi==$risiko && $jmlkpi!=0){
					    $datarisiko=App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->count();
						$stagekp = App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_kp','!=','')->count();
						$stagevk = App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->count();
						$stagemgr = App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->where('date_mgr','!=','')->count();
						$stageupmr = App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_vk','!=','')->where('date_kp','!=','')->where('date_mgr','!=','')->where('date_upmr','!=','')->count();
							if($datarisiko==$stagekp){
								   $data = App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_kp','<>','')->limit(1)->get();
								   
							   }elseif($datarisiko==$stagevk){
								   $data =App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_vk','<>','')->limit(1)->get();
							   }elseif($datarisiko==$stagemgr){
								   $data =App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_mgr','<>','')->limit(1)->get();
							   }elseif($datarisiko==$stageupmr){
								   $data =App\Risikobisnis::where('unit_id',$uni['objectabbr'])->where('periode_id',$periode)->where('date_umpr','<>','')->limit(1)->get();
								   
							   }else{
								   $data=[];
							   }
						if($data!==[]){
								//dd($data);
								echo'
									<table  style="width:98%;margin-left:1%"><br><br>
										<tr>
											<th colspan="13" style="text-align:left">'.$uni['nama'].'<br><br></th>
										</tr>
										<tr>
											<th>Keyperson</th>
											<th>Verifikatur</th>
											<th>Pimpinan Unit</th>
											<th>Pimpinan UPMR</th>
										</tr>
										<tr></tr>
										<tr>
									   
											';
								foreach($data as $no=>$o){
									if($o['date_kp']<>'' && $o['date_vk']<>'' && $o['date_mgr']<>'' && $o['date_upmr']<>''){
										echo'    
										
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_kp"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_vk"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_mgr"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_upmr"])).'</td>
											';
										
									}
									if($o['date_kp']==''){
										echo'    
										<td  class="col-progres">Proses<br>sKeyperson<br></td>
										<td  class="col-progres">Proses<br><br></td>
										<td  class="col-progres">Proses<br><br></td>
										<td  class="col-progres">Proses<br><br></td> 
										
											';
										
									}
									if($o['date_kp']<>'' && $o['date_vk']==''){
										echo'    
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_kp"])).'</td>
											<td  class="col-progres" >Proses<br>Verifikatur<br></td>
											<td  class="col-progres"><br>&nbsp;<br><br></td>
											<td  class="col-progres"><br>&nbsp;<br><br></td>
											';
										
									}
									if($o['date_kp']<>'' && $o['date_vk']<>'' && $o['date_mgr']==''){
										echo'    
										
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_kp"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_vk"])).'</td>
											<td  class="col-progres">Proses<br>Pimpinan Unit<br></td>
											<td  class="col-progres"><br>&nbsp;<br><br></td>
											';
										
									}
									if($o['date_kp']<>'' && $o['date_vk']<>'' && $o['date_mgr']<>'' && $o['date_upmr']==''){
										echo'
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_kp"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_vk"])).'</td>
											<td class="col-selesai" style="text-align:center">'.date('M d Y',strtotime($o["date_mgr"])).'</td>
											<td  class="col-progres">Proses<br>Pimpinan UPMR<br></td>
											';
									}
								}
								}else{
									echo'
									<table  style="width:98%;margin-left:1%">
										<tr>
											<th colspan="13" style="text-align:left">'.$uni['nama'].' <br></th>
										</tr>
										<tr>
											<th>Keyperson</th>
											<th>Verifikatur</th>
											<th>Pimpinan Unit</th>
											<th>Pimpinan UPMR</th>
										   
											
										</tr>
										<tr>
									   <tr><td  class="col-progres">Proses<br>Keyperson<br></td>
											';
								}
						 echo'
							</tr></table>
						';
				 }else{
					 echo'
									<table  style="width:98%;margin-left:1%">
										<tr>
											<th colspan="13" style="text-align:left">'.$uni['nama'].'<br></th>
										</tr>
										<tr>
											<th>Keyperson</th>
											<th>Verifikatur</th>
											<th>Pimpinan Unit</th>
											<th>Pimpinan UPMR</th>
										   
											
										</tr>
										<tr><td  class="col-progres">Proses<br>Keyperson<br></td>
									   
											';
								}
						 echo'
							</tr></table>
						';
				 }
				
		
				
				
				   
                
	
		
}

function last_periode(){
	
		}


?>