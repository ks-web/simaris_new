@extends('layouts.app')
<style>
    th{
        font-size:12px;
        background:#b0e6e6;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    td{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:3px;
    }
    .ttd{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    
</style>
@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
            
               
                <div class="box-header" style="margin-bottom:0%">
                    
                    <h3 class="box-title">
                    <!-- <span class="btn btn-success btn-sm" onclick="tambah()"><i class="fa fa-plus"></i> Tambah Baru</span> -->
                    <!-- <span class="btn btn-primary btn-sm" onclick="importdata()"><i class="fa fa-clone"></i> Import</span> -->
                    </h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 400px;">
                            <select name="table_search" style="display:inline" id="periode" onchange="cari(this.value)" class="form-control pull-right" placeholder="Search">
                           
                                        @foreach(periode_sts() as $peri)
                                            <option value="{{$peri['id']}}" @if($periode==$peri['id']) selected @endif >[{{$peri['tahun']}}] {{$peri['name']}}</option>
                                        @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default" ><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="tampilkan" style="padding:100px">
                
                    
                
                </div>
            
            </div>
          <!-- /.box -->
        </div>
    </div>
</section>



@endsection


@push('datatable')
    <script>
        function tambah(){
            $('#modal-default').modal({backdrop: 'static', keyboard: false});
        }
        function importdata(){
            $('#modal-import').modal({backdrop: 'static', keyboard: false});
        }

        var periode=$('#periode').val();
       
       
        $(document).ready(function() {
            
            $.ajax({
               type: 'GET',
               url: "{{url('view_data_traking')}}?periode="+periode,
               data: "id=id",
               beforeSend: function(){
                    $("#tampilkan").html('<center><img src="{{url(url_link().'/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
               },
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").html(msg);
                  
               }
           });
            

        });

        function cek_alasan(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/ulasan')}}/"+a+"?role=1",
               data: "id=id",
               success: function(msg){
                   $('#modalulasan').modal({backdrop: 'static', keyboard: false});
                   $("#tampilkanulasan").html(msg);
                   
                  
               }
           });
            
        }

        function view_dampak_new(){
           var peluang=$('#peluang_id').val();
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/view_dampak_new')}}?peluang="+peluang,
               data: "id=id",
               success: function(msg){
                   $('#modaldampaknya').modal({backdrop: 'static', keyboard: false});
                   $("#tampilkanulasan").html(msg);
                   
                  
               }
           });
            
        }

        function pilihdampak(nama_kriteria,kriteria_id,kategori_id,dampak_id,warna,tingkat){
            
            $('#modaldampak').modal('hide');
            $('#nama_kriteria').val(nama_kriteria);
            $('#kriteria_id').val(kriteria_id);
            $('#kategori_id').val(kategori_id);
            $('#dampak_id').val(dampak_id);
            $('#nama_kriteriad').val(nama_kriteria);
            $('#kriteria_idd').val(kriteria_id);
            $('#kategori_idd').val(kategori_id);
            $('#dampak_idd').val(dampak_id);
            $('#warna').html('<span class="btn btn-'+warna+' btn-sm">'+tingkat+'</span>');
            $('#warnaedit').html('<span class="btn btn-'+warna+' btn-sm">'+tingkat+'</span>');
        }

        function cari(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('view_data_traking')}}?periode="+a,
               data: "id=id",
               beforeSend: function(){
                    $("#tampilkan").html('<center><img src="{{url(url_link().'/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
               },
               success: function(msg){
                   $("#tampilkan").html(msg);
                  
               }
           });
            
        }

        function cari_unit(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kpi/cari_unit')}}?name="+a,
               data: "id=id",
               success: function(msg){
                   var data=msg.split('/');
                   $("#nama_unit").val('['+data[1]+']'+data[0]);
                   $("#unit_id").val(data[1]);
                  
               }
           });
            
        }

        function cek_dampak(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/view_dampak')}}?dampak="+a,
               data: "id=id",
               success: function(msg){
                   $("#tampildampak").html(msg);
                  
               }
           });
            
        }

        function edit_sumber(id,no){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/edit_sumber')}}/"+id+"?no="+no,
               data: "id=id",
               success: function(msg){
                   $("#modaleditsumber").modal({backdrop: 'static', keyboard: false});
                   $("#tampilkanubahsumber").html(msg);
                  
               }
           });
            
        }
        function cek(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kasir/cek')}}/"+a,
               data: "id=id",
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").load("{{url('kasir/view_data_user?unit='.'&periode='.$periode)}}");
                  
               }
           });
            
        }
        function uncek(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kasir/uncek')}}/"+a,
               data: "id=id",
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").load("{{url('kasir/view_data_user?unit='.'&periode='.$periode)}}");
                  
               }
           });
            
        }

        function sumber_detail(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/sumber_verifikatur')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkanubah_detail").html(msg);
                   $('#modalubah_detail').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }
        function dampaknya(){
           
            var peluang=$('#peluang_idnya').val();
            if(peluang==''){
                alert('Pilih peluang terlebih dahulu')
            }else{
                $.ajax({
                    type: 'GET',
                    url: "{{url('risiko/view_dampak_new')}}?peluang="+peluang,
                    data: "id=id",
                    success: function(msg){
                        $('#modaldampak').modal({backdrop: 'static', keyboard: false});
                        $("#view_dampak_new").html(msg);
                        
                        
                    }
                });
            } 
            
        }
        function editdampaknya(){
           
            var peluang=$('#peluang_idnyaedit').val();
            if(peluang==''){
                alert('Pilih peluang terlebih dahulu')
            }else{
                $.ajax({
                    type: 'GET',
                    url: "{{url('risiko/view_dampak_new')}}?peluang="+peluang,
                    data: "id=id",
                    success: function(msg){
                        $('#modaldampak').modal({backdrop: 'static', keyboard: false});
                        $("#view_dampak_new").html(msg);
                        
                        
                    }
                });
            } 
            
        }

        function kirim_email()
        {
           var idnya=$('#idnya').val();
           $.ajax({
               type: 'GET',
               url: "{{url('kasir/kirim_email')}}/"+idnya,
               data: "id=id",
               beforeSend: function(){
                    $('#modalloading').modal({backdrop: 'static', keyboard: false});
               },
               success: function(msg){
                   $('#modalloading').modal('hide');
                   $("#notifcetak").html(msg);
                  
               }
           });
            
        }

        function hapus(a){
            if (confirm('Apakah yakin akan menghapus data ini?')) {
                
                $.ajax({
                    type: 'GET',
                    url: "{{url('risiko/hapus_risiko')}}/"+a,
                    data: "id="+a,
                    beforeSend: function(){
                                $('#modalloading').modal({backdrop: 'static', keyboard: false});
                        },
                    success: function(msg){
                           location.reload();
                    }
                });
            }

        }
        
        function sumber(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/sumber')}}/"+a,
               data: "id=id",
               success: function(msg){
                   data=msg.split('||');
                   $("#tampilkanubah").html(data[0]);
                   $("#tampilkanubah_data").html(data[1]);
                   $('#modalubah').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }

        function ubah(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/ubah')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkanubahrisiko").html(msg);
                   $("#notifikasiubah_risiko").html('');
                   $('#modalubahrisiko').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }
        function selesai(a){
            if (confirm('Apakah yakin data ini sudah selesai?')) {
                $.ajax({
                    type: 'GET',
                    url: "{{url('risiko/ubah_sts')}}/"+a,
                    data: "id=id",
                    success: function(msg){
                        location.reload();
                        
                    }
                });
            }
            
        }

        function cek_modal(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kpi/ubah_sts')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkanubah_sts").html(msg);
                   $('#modalubah_sts').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }
        function hapus_sumber(no,id){
           
           $.ajax({
               type: 'GET',
               url: "{{url('risiko/hapus_sumber')}}?no="+no+"&id="+id,
               data: "no="+no+"&id="+id,
               success: function(msg){
                        $.ajax({
                            type: 'GET',
                            url: "{{url('risiko/sumber')}}/"+msg,
                            data: "id=id",
                            success: function(det){
                                has=det.split('||');
                                $("#tampilkanubah").html(has[0]);
                                $("#tampilkanubah_data").html(has[1]);
                                $('#notifikasiubah').html('');
                                
                            }
                        });
                  
               }
           });
            
        }

        function cetak(a){
           
            $("#printableArea").load("{{url('kasir/cetak')}}/"+a);
            $('#modalcetak').modal({backdrop: 'static', keyboard: false});
            
        }

        function simpan_data(){
            var form=document.getElementById('mysimpan_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/risiko/simpan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(msg){
                        if(msg=='ok'){
                            location.reload();
                               
                        }else{
                            $('#simpan_data').show();
                            $('#notifikasi').html(msg);
                        }
                        
                        
                    }
                });

        } 

        function import_simpan_data(){
            var form=document.getElementById('myimport_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/kpi/import')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#modalloading').modal({backdrop: 'static', keyboard: false});
                    },
                    success: function(msg){
                        data=msg.split('=');
                        
                        if(data[0]=='<p style'){
                            $('#modalloading').modal('hide');
                            $('#simpan_data').show();
                            $('#notifikasi_import').html(msg);
                            
                               
                        }else{
                            $('#modal-import').modal('hide');
                            $('#modalloading').modal('hide');
                            $("#tampilkan").load("{{url('kpi/view_data_user?unit='.'&periode='.$periode)}}");
                        }
                        
                        
                    }
                });

        } 

        function ubah_data(){
            var form=document.getElementById('myubah_data');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/risiko/ubah_data')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(msg){
                        data=msg.split('||');
                        if(data[0]=='ok'){
                            $.ajax({
                                type: 'GET',
                                url: "{{url('risiko/sumber')}}/"+data[1],
                                data: "id=id",
                                success: function(det){
                                    has=det.split('||');
                                    $("#tampilkanubah").html(has[0]);
                                    $("#tampilkanubah_data").html(has[1]);
                                    $('#notifikasiubah').html('');
                                    
                                }
                            });
                        }else{
                            $('#notifikasiubah').html(msg);
                        }
                        
                        
                    }
                });

        } 

        function ubah_data_sumber(){
            var form=document.getElementById('myubah_datasumber');
                
                $.ajax({
                    type: 'POST',
                    url: "{{url('/risiko/ubah_data_sumber')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(msg){
                        data=msg.split('||');
                        if(data[0]=='ok'){
                            $('#modaleditsumber').modal('hide');
                            $('#notifikasiubahsumber').html('');
                            $.ajax({
                                type: 'GET',
                                url: "{{url('risiko/sumber')}}/"+data[1],
                                data: "id=id",
                                success: function(det){
                                    has=det.split('||');
                                    $("#tampilkanubah").html(has[0]);
                                    $("#tampilkanubah_data").html(has[1]);
                                    $('#notifikasiubah').html('');
                                    
                                }
                            });
                        }else{
                            $('#notifikasiubahsumber').html(msg);
                        }
                        
                        
                    }
                });

        } 
        function ubah_data_risiko(){
            var form=document.getElementById('myubah_data_risiko');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/risiko/ubah_data_risiko')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(msg){
                        
                        if(msg=='ok'){
                            location.reload();
                        }else{
                            $('#notifikasiubah_risiko').html(msg);
                        }
                        
                        
                    }
                });

        } 


        function ubah_data_sts(){
            var form=document.getElementById('myubah_data_sts');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/kpi/ubah_data_sts')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#modalloading').modal({backdrop: 'static', keyboard: false});
                    },
                    success: function(msg){
                        
                        if(msg=='ok'){
                            $('#modalubah_sts').modal('hide');
                            $('#modalloading').modal('hide');
                            $("#tampilkan").load("{{url('kpi/view_data_user?unit='.'&periode='.$periode)}}");
                        }else{
                            $('#modalloading').modal('hide');
                            $('#notifikasiubahsts').html(msg);
                        }
                        
                        
                    }
                });

        } 

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    
@endpush