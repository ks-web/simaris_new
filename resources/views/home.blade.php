@extends('layouts.app')
<style>
    th{
        font-size:12px;
        background:#b0e6e6;
		color : black;
    }
    td{
        font-size:12px;
    }
    .ttd{
        border:solid 10x #000;
        background:#fff;
        padding:5px;
    }
	a:link {
  color: black;
  background-color: transparent;
  text-decoration: none;
}
a:visited {
  color: black;
  background-color: transparent;
  text-decoration: none;
}
a:hover {
  color: red;
  background-color: transparent;
  text-decoration: underline;
}
a:active {
  color: yellow;
  background-color: transparent;
  text-decoration: underline;
}

</style>
@section('content')


<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <div class="col-md-12">
            <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                 <!--    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username">{{Auth::user()['name']}}</h3>
                        <h5 class="widget-user-desc">{{Auth::user()['email']}}</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{{url(url_link().'/img/akun.png')}}" alt="User Avatar">
                    </div> -->
					<div class="col-md-12" style="padding-top:50px; ">
					
					<div class="col-md-4" style="padding-top:50px; ">	
						<div style="text-align:left; background-color:white;">
						<table style=" border:solid 0px #000;padding:0px;">
						<tr>
							<td rowspan="2" style=" border:solid 0px #000;padding:0px"><center><img src="{{url(url_link().'/img/Picture1.png')}}" width="70" style="padding-top:3px;padding-left:0px;padding-right:0px"></center></td>
							<td style="font-size:12pt;text-align:center;color:#002366;border:solid 0px #000;padding-top:10px;padding-bottom:0px"><b>Sistem Manajemen Risiko</b></td>
						</tr>
						<tr>							
							<td style="font-size:12pt;text-align:center;color:#002366;border:solid 0px #000;padding-top:0px"><b>(SMR)</br></td>
						</tr>
						<tr rowspan="2" style=" border:solid 0px #000;padding:0px">
							<td colspan="2" style=" border:solid 0px #000;padding:0px">
								<p style="text-align:justify;padding: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suatu sistem aplikasi informasi yang dibangun untuk pengelolaan manajemen risiko Perseroan secara terintegrasi diseluruh unit kerja, hal ini dilakukan untuk memudahkan didalam monitoring dan pelaporan hasil analisis manajemen risiko dari berbagai unit bisnis yang tersebar disemua lini Perseroan yang terdiri dari unit kerja setingkat Divisi dan setingkat Subdit. Dengan memanfaatkan SMR seluruh pelaporan risiko dapat dengan cepat dilaporkan secara on time dan monitoring profil risiko Perseroan dapat terpetakan secara online dengan utilisasi SDM yang minimal.</p>
							</td>
							</tr>
						
						</table>
						
						</div>						
					</div>	
					
					<div class="col-md-8" style="padding-top:0">					
						<div>
							<ol style="list-style:none; padding-left:100px;">
								<li style="padding-bottom:25px"><img src="{{url(url_link().'/img/Picture2.png')}}" width="70">&nbsp;&nbsp;<b><a href="#" onclick="pedoman()">Pedoman Manajemen Risiko</a></b></li>
								
								<li style="padding-bottom:25px"><img src="{{url(url_link().'/img/Picture3.png')}}" width="70">&nbsp;&nbsp;<b><a href="#" onclick="guideline()">Petunjuk Pengunaan Sistem Manajemen Risiko</a></b></li>
								<li style="padding-bottom:25px"><img src="{{url(url_link().'/img/Picture4.png')}}" width="70">&nbsp;&nbsp;<b><a href="#" onclick="lastprogress()">Pelaporan Periode Sebelumnya</a></b></li>
								<li style="vertical-align : middle;text-align:center;background-color:white;padding:10px"><b>Progress Laporan {{ periode_aktif()['name']}}</b></li>
								<div class="col-md-12">
								
								{{ stage(periode_aktif()['id'])}}
						
								</div>
							</ol>
						<div>
						<div>
							<ol style="list-style:none; padding-left:200px;"></ol>
						</div>
					</div>

                    <!-- <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Administrator<br><br></h5>
                                    <span class="description-text">{!!cek_role(admin())!!}</span>
                                </div>
                            </div>
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Keyperson<br><br></h5>
                                    <span class="description-text">{!!cek_role(keyperson())!!}</span>
                                </div>
                            </div>
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Verifikatur<br><br></h5>
                                    <span class="description-text">{!!cek_role(verifikatur())!!}</span>
                                </div>
                            </div>
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Pimpinan unit<br><br></h5>
                                    <span class="description-text">{!!cek_role(pimpinanunit())!!}</span>
                                </div>
                            </div>
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">UPMR<br><br></h5>
                                    <span class="description-text">{!!cek_role(managergcg())!!}</span>
                                </div>
                            </div>
                            <div class="col-sm-2 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Pimpinan Subdit<br><br></h5>
                                    <span class="description-text">{!!cek_role(pimpinansubdit())!!}</span>
                                </div>
                            </div>
                            
                        </div> -->
                    <!-- /.row -->
                    </div>
                </div>
            <!-- /.widget-user -->
            </div>
            
          </div>
          <!-- /.box -->
        </div>
    </div>
	<div class="modal fade" id="pedoman" role="dialog">
    <div class="modal-dialog " style="width:90%">
        <div class="modal-content" style="margin-top:-10px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Tutup</button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="{{url(url_link().'/img/Buku Pedoman MR Share.pdf')}}" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
                
            
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  </div>
  
  <!-- Modal start SMR Guideline -->
  <div class="modal fade" id="guideline" role="dialog">
    <div class="modal-dialog " style="width:90%">
        <div class="modal-content" style="margin-top:-10px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Tutup</button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="{{url(url_link().'/img/SMR - User Guideline.pdf')}}" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">               
            
            </div>
        </div>
    </div>
  </div>
  <!-- Modal end SMR Guideline -->
  
   <!-- Modal start Last Progress -->
  <div class="modal fade" id="lastprogress" role="dialog">
    <div class="modal-dialog " style="width:90%">
        <div class="modal-content" style="margin-top:-10px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">Tutup</button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
				{{last_periode()}}
                </div>
            </div>
            <div class="modal-footer">               
            
            </div>
        </div>
    </div>
  </div>
  <!-- Modal end Last Progress -->
  
</div>
</section>

@endsection
@push('datatable')
    <script>
	function pedoman(){
            $('#pedoman').modal({backdrop: 'static', keyboard: false});
        }
	function guideline(){
            $('#guideline').modal({backdrop: 'static', keyboard: false});
        }
	function lastprogress(){
            $('#lastprogress').modal({backdrop: 'static', keyboard: false});
        }
    </script>
@endpush
