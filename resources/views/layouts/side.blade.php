<ul class="sidebar-menu" data-widget="tree">
    
    <li><a href="{{url('/')}}"><i class="fa fa-home"></i>Halaman Utama</a></li>
    
    
    @if(admin()>0)
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Otorisasi Pengguna</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{url('user/')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Pengguna</a></li>
                <li><a href="{{url('admin/')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Role Akses</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Master</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{url('periode/')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Periode</a></li>
                <li><a href="{{url('unit/')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Unit</a></li>
                <li><a href="{{url('kpi/')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> KPI</a></li>
            </ul>
        </li>
        
    @endif

    @if(pimpinanunit()>0)
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>KPI Pimpinan</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_pimpinanunit() as $get_pin)
                <li><a href="{{url('kpi/pimpinanunit?unit='.$get_pin['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i> {{substr(cek_unit($get_pin['unit_id'])['nama'],0,25)}}</a></li>
             @endforeach
            </ul>
        </li>
        
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Risiko Bisnis Pimpinan</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_pimpinanunit() as $get_key)
                <li><a href="{{url('risiko/pimpinanunit?unit='.$get_key['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i> {{substr(cek_unit($get_key['unit_id'])['nama'],0,25)}}</a></li>
             @endforeach
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Laporan Risiko Pimpinan</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_pimpinanunit() as $get_key)
                <li><a href="{{url('laporan_risiko?unit='.$get_key['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i> {{substr(cek_unit($get_key['unit_id'])['nama'],0,25)}}</a></li>
             @endforeach
            </ul>
        </li>
    @endif
    @if(pimpinangcg()>0)
        
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Risiko Bisnis Pimpinan UPMR</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="{{url('risiko/pimpinangcg')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Risiko Bisnis Proses</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Laporan Risiko UPMR</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="{{url('laporan_risiko/user')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Laporan Risiko Bisnis</a></li>
            </ul>
        </li>
    @endif
    @if(pimpinansubdit()>0)
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Laporan Persubdit</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_pimpinansubdit() as $get_sub)
                <li><a href="{{url('laporan_risiko/pimpinansubdit?unit='.$get_sub['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i> {{substr(cek_unit($get_sub['unit_id'])['nama'],0,25)}} </a></li>
             @endforeach
            </ul>
        </li>
        @foreach(unit_pimpinansubdit() as $get_sub)
        <!-- <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Risiko Bisnis Subdit {{cek_unit($get_sub['unit_id'])['singkatan']}}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            
                @foreach(get_unit_pimpinansubdit($get_sub['unit_id']) as $get_sub_get)
                    <li><a href="{{url('risiko/pimpinansubdit?unit='.$get_sub_get['objectabbr'])}}" style="font-size: 12px">&nbsp;<i class="fa fa-minus text-yellow"></i>{{$get_sub_get['nama']}} </a></li>
                @endforeach
             
             
            </ul>
        </li> -->
        @endforeach
    @endif

    @if(keyperson()>0)
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>KPI </span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_keyperson() as $get_key)
                <li><a href="{{url('kpi/keyperson?unit='.$get_key['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i>KPI {{cek_unit($get_key['unit_id'])['nama']}}</a></li>
             @endforeach
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Risiko Bisnis Proses</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_keyperson() as $get_key)
                <li><a href="{{url('risiko?unit='.$get_key['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i>{{cek_unit($get_key['unit_id'])['nama']}}</a></li>
             @endforeach
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Laporan Risiko Bisnis Proses</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
             @foreach(unit_keyperson() as $get_key)
                <li><a href="{{url('laporan_risiko?unit='.$get_key['unit_id'])}}">&nbsp;<i class="fa fa-minus text-yellow"></i> {{cek_unit($get_key['unit_id'])['nama']}}</a></li>
             @endforeach
            </ul>
        </li>
    @endif
    @if(verifikatur()>0)
        <!-- <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Master </span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{url('kpi')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> KPI</a></li>
            </ul>
        </li> -->
        <li class="treeview">
            <a href="#">
            <i class="fa fa-folder text-yellow"></i> <span>Risiko Bisnis</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                    <li><a href="{{url('risiko/verifikatur')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Risiko Bisnis</a></li>
                    <li><a href="{{url('laporan_risiko/user')}}">&nbsp;<i class="fa fa-minus text-yellow"></i> Laporan Risiko Bisnis</a></li>
					 <li><a href="{{url('/traking')}}"><i class="fa fa-folder text-yellow"></i>Monitoring Risiko Bisnis</a></li> 
            
            </ul>
        </li>
    @endif
   

</ul>